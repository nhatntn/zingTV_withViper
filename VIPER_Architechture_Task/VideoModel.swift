//
//  VideoModel.swift
//  VIPER_Architechture_Task
//
//  Created by ArrowTN on 08/09/2017.
//  Copyright © 2017 ArrowTN. All rights reserved.
//

import Foundation
import SwiftyJSON
import ObjectMapper

struct VideoModel {
    var id = 0
    var name = ""
    var eps = ""
    var videoUrl = ""
    var imgUrl = ""
    var des = ""
    var type = ""
    var schedule = ""
}

extension VideoModel: Mappable {
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        id          <- map["id"]
        name        <- map["name"]
        eps         <- map["eps"]
        videoUrl    <- map["url"]
        imgUrl      <- map["img"]
        des         <- map["des"]
        type        <- map["type"]
        schedule    <- map["schedule"]
    }
    
}


