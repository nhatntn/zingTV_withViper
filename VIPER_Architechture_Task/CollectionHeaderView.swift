//
//  CollectionHeaderView.swift
//  VIPER_Architechture_Task
//
//  Created by ArrowTN on 15/09/2017.
//  Copyright © 2017 ArrowTN. All rights reserved.
//

import UIKit

class CollectionHeaderView: UICollectionReusableView {
    // MARK: *** UI Elements
    @IBOutlet weak var title: UILabel!
}
