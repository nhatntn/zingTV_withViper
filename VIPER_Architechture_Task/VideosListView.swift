//
//  VideosListView.swift
//  VIPER_Architechture_Task
//
//  Created by ArrowTN on 11/09/2017.
//  Copyright © 2017 ArrowTN. All rights reserved.
//

import UIKit
import PKHUD

class VideosListView: UIViewController {
    // MARK: *** UI Elements
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var mainView: UIView!
    
    var presenter: VideosListPresenterProtocol?
    // MARK: *** Data model
    var videoList: [VideoModel] = []
    
    // MARK: *** UI Events
    
    // MARK: *** UIViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        self.navigationController?.navigationBar.barStyle = UIBarStyle.default
    }
}

extension VideosListView: VideosListProtocol {
    func showVideos(with videos: [VideoModel]) {
        videoList = videos
        collectionView.reloadData()
    }
    
    func showError() {
        HUD.flash(.label("Internet not connected"), delay: 2.0)
    }
    
    func showLoading() {
        HUD.show(.progress)
    }
    
    func hideLoading() {
        HUD.hide()
    }
}

extension VideosListView: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2 //Yêu cầu collection View có 2 sections
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10 //Yêu cầu mỗi section sẽ chứa 10 Items
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionElementKindSectionHeader:
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "Header", for: indexPath) as! CollectionHeaderView
            if indexPath.section == 0 {
                headerView.title.text = "VIDEO MỚI CẬP NHẬT"
            }
            else if indexPath.section == 1{
                headerView.title.text = "PHIM MỚI THÁNG 8"
            }
            return headerView
        case UICollectionElementKindSectionFooter:
            let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "Footer", for: indexPath)
            if indexPath.section == 1 {
                footerView.backgroundColor = UIColor.white
            }
            return footerView
        default:
            assert(false, "Unexpected element kind")
        }
    }
    
    // Chỉ định cách hiển thị từng Item của Collection View
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "VideoCollectionCellID", for: indexPath) as! VideoCollectionCell
        if indexPath.section == 0 {
            let video = videoList[indexPath.row]
            cell.set(forVideos: video)
            return cell
        }else{
            let video = videoList[indexPath.row + 10] //Lấy 10 Items kể từ Item thứ 10 trong danh sách videoList lấy được từ file Json
            cell.set(forVideos: video)
            return cell
        }
    }
    // Xử lí khi có sự kiện lựa chọn
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            presenter?.showVideoDetail(forVideo: videoList[indexPath.row])
        }
        else {
            presenter?.showVideoDetail(forVideo: videoList[indexPath.row+10])
        }
    }
    
    // Xử lí việc hiển thị cell ở các màn hình kích thước khác nhau
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width  = (self.view.frame.size.width - 30)/2;
        let height = CGFloat((width - 6)/1.8 + 32);
        // in case you you want the cell to be 40% of your controllers view
        return CGSize(width: width, height: height)
    }
}
